package main

import (
	"fmt"
)

func main() {
	//buffered channel (canal con buffer)
	//send only channel
	c := make(chan int)

	//enviar
	go func() {
		for i := 0; i < 5; i++ {
			c <- i
		}
		close(c)
	}()

	for v := range c {
		fmt.Println(v)
	}

	fmt.Println("Finalizando")
}
